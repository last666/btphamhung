# 2180607542 - Phạm Hùng #

# UserStory #
| **Title**               | Delete subject information                      |
| ----------------------- | ----------------------------------------------- |
| **Value Statement**     |   -  As a user, I want to delete subject classes to update my subject class information | 
|                         |   - Delete subject classes                      |
| **Acceptance Criteria** |   - Given that the class information need to be deleted |
|						  |	  - When the user is logged into the system with their account |
|                         |   - When users search and select the subject class that needs to be deleted to update information |
|                         |   - Then the system must display all information about that subject class |
|                         |   - And user can delete the searched subject class |
|                         |   - The user must confirm when they have completed deleting the subject class |
|                         |   - Then the system needs to update subject class information after the user has deleted it |
|                         |   - Then the system must notify the user that updating the subject class information has been successful |
|                         |   - Ensure that updating subject class information can be conveniently done from the user interface |
| **Definition of Done**  |   * Acceptance Criteria Met                     |
|                         |   * Test Cases Passed                           |
|                         |   * Code Reviewed                               |
|                         |   * Package Into Specific Folders               |
|                         |   * Synchronize Folders Between Devices         | 
|                         |   * Product Owner Accepts User Story            |
| **Owner**               |    Phạm Hùng                                    |
| **Interation**          |   * Unscheduled                                 |
| **Estimate**            |   * 5 points                                    |

![xoa](XoaLopMonHoc.png)


# 2180608365 - Phan Thanh Chieu Duc #
# UserStory #

| **Title**               |   Search classes                    |
| ----------------------- | ----------------------------------------------- |
| **Value Statement**     |   -  As a user, I want to search for subject classes by class code and teacher | 
|                         |   - Search classes                  |
| **Acceptance Criteria** |   - When the user wants to search for subject class , make sure the school subject appears |
|                         |    Students can enter class encoding to search |
|                         |    The system will search and display all classes with duplicate code matching the user input|
|                         |    Students can enter member names to search|
|                         |    The system will search and display all subjects taught by students whose names match the user input|
|                         |    Search results are displayed clearly and easily to read, including information about the class, class code and student name|
|                         |    The system allows users to search based on either class code, name, or both|
|                         |    Other feature options such as sorting results by different criteria or expanded search may be provided|
|                         |    Ensure that only users with new permissions can access classroom and teacher information|
|                         |    The system must handle problems correctly, for example if no results are found or there is an error connection|
| **Definition of Done**  |    Unit Tests Passed                            |
|                         |    Acceptace Criteria Met                       |
|                         |    Code Reviewed                                |
|                         |    Functional Tests Passed                      |
|                         |    Non-Functional Requirement Met               |
|                         |    Product Owner Accepts User Story             |
| **Owner**               |    Phan Thanh Chieu Duc                         |
| **Interation**          |    Unscheduled                                  |
| **Estimate**            |    5 points                                     |

![xoa](duc.jpg)






# 2180608294 - Lê Trần Xuân Kiên#

# UserStory #
| **Title**               |  Check class and subject details                         |
| ----------------------- | ----------------------------------------------- |
| **Value Statement**     |    As a user, I want to go to the detailed information page to make it easier to check | 
|                         |    Displays all information for viewing         |
| **Acceptance Criteria** |    The home page or topic list must provide a link or way for students to get to the detailed information page     |
|                         |    Theme details page should display important information, including the theme name, description, images, or any other necessary information   |
|                         |    The detail page needs to be designed so that users can easily access it and not have difficulty finding it      |
|                         |    The details page should allow for easy student interaction, such as scrolling the page, zooming in on images, or viewing related information |
|                         |    Information on the details page should be displayed clearly and formatted so that it is easy to read and review |
|                         |    The detail page needs to be compatible with different devices, including desktop and mobile |
|                         |    The detail page can integrate related information such as articles, images, or other links to provide additional information to students     |
|                         |    Make sure that the insight page view feature works properly and does not cause errors |
| **Definition of Done**  |    Displays the selected topic accurately and appropriately.             |           
|                         |    Navigate back to the previous page or return to the main interface    |
|                         |    Clear instructions on how to access the detailed information page     |
|                         |    Verify this feature is compatible with desktop and mobile devices     |
|                         |    Guaranteed effectiveness                                              |
|                         |    Thoroughly test the navigation as expected                            |
| **Owner**               |    Lê Trần Xuân Kiên                           |
| **Interation**          |    Unscheduled                                 |
| **Estimate**            |    5 points                                    |

![xoa](1.jpg)




# 2180608335 - Trần Lê Vỹ #

# UserStory #
| **Title**               |  See topic information                          |
| ----------------------- | ----------------------------------------------- |
| **Value Statement**     |    As a user, I want to display subject class , information for easy viewing | 
|                         |    Display subject class information            |
| **Acceptance Criteria** |    When I access the application, I should see a menu or navigation option that leads to the "Subject Class" information |
|                         |    Clicking on the "Subject Class" menu should display a clear and organized list of subject classes |
|                         |    The subject class information should include the class name, class description, and any relevant details |
|                         |    The information should be presented in a user-friendly and easy-to-read format, such as a table or list|
|                         |    The displayed information should be up-to-date and reflect the latest data|
|                         |    There should be a way to filter or search for specific subject classes if there are many of them |
|                         |    The user interface should be responsive and work well on different devices and screen sizes |
|                         |    Any user, whether they are familiar with the system or not, should be able to easily find and view subject class information |
| **Definition of Done**  |    Acceptance Criteria Met                     |           
|                         |    Test Cases Passed                           |
|                         |    Code Reviewed                               |
|                         |    Package Into Specific Folders               |
|                         |    Synchronize Folders Between Devices         |
|                         |    Product Owner Accepts User Story            |
| **Owner**               |    Lê Vỹ CAPTAIN LeVi                          |

![thong tin Lop](thongtinlop.png)

# 2180608335 - Nguyễn Lưu Hoàng Gia #

# UserStory #
| **Title**               |  access the subject class details page          |
| ----------------------- | ----------------------------------------------- |
| **Value Statement**     |   - As a user, I would like to access the subject class details page to make testing easier | 
|                         |   - Display subject class information            |
| **Acceptance Criteria** |   - There must be a visible student interface component to allow users to access the subject class details page |
|                         |   - They will be taken to the course details page after interacting with the student interface component |
|                         |   - The subject details page must have accurate and pertinent information in order for students to comprehend the subject |
|                         |   - The way students access detail pages and view data should be consistent with how well the program works |
|                         |   - Detail pages should be responsive and compatible with multiple devices and screen sizes |
|                         |   - After studying the subject specifics, students will have the choice of going back to the main interface or the prior location |
|                         |   - Verify and confirm that students may access the details page without any issues |
|                         |   - Ensure that reaching the details page won't result in performance hiccups or sluggish response times |
|                         |   - Provide  or instructions as necessary to assist students in accessing the class topic detail page |
| **Definition of Done**  |    * Acceptance Criteria Met                     |           
|                         |    * Test Cases Passed                           |
|                         |    * Code Reviewed                               |
|                         |    * Package Into Specific Folders               |
|                         |    * Synchronize Folders Between Devices         |
|                         |    * Product Owner Accepts User Story            |
| **Owner**               |    Nguyễn Lưu Hoàng Gia                        |
| **Interation**          |    Unscheduled                                 |
| **Estimate**            |    5 points                                    |


![Gia](gia.jpg)
